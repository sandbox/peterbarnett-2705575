<?php
/**
 * The "example_behavior_plugin" Entity Reference plugin.
 */
class EntityReferenceOGDefaultGroupBehavior extends EntityReference_BehaviorHandler_Abstract {

  /**
   * Implements EntityReference_BehaviorHandler_Abstract::access().
   */
  public function access($field, $instance) {
    return $field['settings']['handler'] == 'og' || strpos($field['settings']['handler'], 'og_') === 0;
  }


  /**
   * Generate a settings form for this handler.
   */
  public function settingsForm($field, $instance) {
    $form['hide_for_default'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable editing when default value is provided.'),
    );
    return $form;
  }

}