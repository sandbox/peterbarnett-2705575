<?php

$plugin = array(
  'title' => t('OG Default Group'),
  'description' => t('When only one group is available, set as default value.'),
  'class' => 'EntityReferenceOGDefaultGroupBehavior',
  'behavior type' => 'instance',
);
